from django.apps import AppConfig


class MailservConfig(AppConfig):
    name = 'mailServ'
