from django.core.mail import send_mail
from django.http import HttpResponse, HttpRequest
from dawMail import settings
import json


def index(request):
    if request.method == "POST":
        req = json.loads(request.body.decode())
        send_mail(
            'MailSubject',
            req['text'],
            settings.EMAIL_HOST_USER,
            [req['mail']], # adresa care primeste mail
            fail_silently=False,
        )
    return HttpResponse(status=200)
