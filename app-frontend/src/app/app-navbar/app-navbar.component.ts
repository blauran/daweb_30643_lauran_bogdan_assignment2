import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  template: `
  <div class="page-header" style="background-color:rgb(220, 220, 220);">
        <div class="row flex-nowrap justify-content-between align-items-center">
          <div class="col-4 text-center">
          <button (click)="onChangeLanguage()" class="btn btn-outline-secondary">{{ 'changeLanguage' | translate }}</button>
          </div>
          </div>
  </div>
  <nav class="navbar sticky-top navbar-expand navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/home']" routerLinkActive="active" href="#"> {{ 'navbar.acasa' | translate }} <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/news']" routerLinkActive="active" href="#">{{ 'navbar.noutati' | translate }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/about']" routerLinkActive="active" href="#">{{ 'navbar.despre' | translate }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/coordonator']" routerLinkActive="active" href="#">{{ 'navbar.coordonator' | translate }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/contact']" routerLinkActive="active" href="#">{{ 'navbar.contact' | translate }}</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['/profile']" routerLinkActive="active" href="#">{{ 'navbar.profil' | translate }}</a>
      </li>
    </ul>
  </div>
</nav>

  `,
  styleUrls: ['./app-navbar.component.css']
})

export class AppNavbarComponent implements OnInit {

  isEnglish = false;

  constructor(private service: TranslateService) {}

  onChangeLanguage() {
    if (this.isEnglish) {
      this.service.use('ro');
    } else {
      this.service.use('en');
    }

    this.isEnglish = !this.isEnglish;
  }

  ngOnInit() {
  }

}
