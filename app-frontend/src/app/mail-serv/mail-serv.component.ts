import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmailValidator } from '@angular/forms';

@Component({
  selector: 'app-mail-serv',
  templateUrl: './mail-serv.component.html',
  styleUrls: ['./mail-serv.component.css']
})
export class MailServComponent implements OnInit {

  constructor(private _http: HttpClient) { }

  
  submit(text: string, mail: string){
    this._http.post('http://127.0.0.1:8000/mailServ/contact',{text, mail}).toPromise().then(() => alert('Mail sent')).catch(() => alert('Mail can\'t be sent'))
  }

  ngOnInit() {
  }

}
